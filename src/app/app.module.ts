import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';



import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NewAccountComponent } from './new-account/new-account.component';
import { HomeComponent } from './home/home.component';
import { EmbassyComponent } from './embassy/embassy.component';
import { EmbassyListComponent } from './embassy-list/embassy-list.component';
import { EmbassyFormComponent } from './embassy-form/embassy-form.component';
import { EmbassyMembersComponent } from './embassy-members/embassy-members.component';
import { HoldableDirective } from './holdable.directive';
import { EmbassyService } from './embassy.service';
import { HttpClientModule } from '@angular/common/http';
import { UserFormComponent } from './user-form/user-form.component';
import { UserComponent } from './user/user.component';
import { UserService } from './user.service';



const appRoutes: Routes = [
  { path: '', redirectTo: '/App/Embassy', pathMatch: 'full' },
  { path: 'Login',component:LoginComponent },
  { path: 'ForgotPassword', component:ForgotPasswordComponent},
  { path: 'App', component:HomeComponent,children:[
    {
      path: 'Embassy', 
      component:EmbassyComponent,
      children:[
        {
          path: '', 
          component:EmbassyListComponent,
        },
        {
          path: 'View/:id', 
          component:EmbassyFormComponent,
          data:{
            mode:"VIEW"
          }
        },
        {
          path: 'Update/:id', 
          component:EmbassyFormComponent,
          data:{
            mode:"UPDATE"
          }
        },
        {
          path: 'Members/:id', 
          component:EmbassyMembersComponent
        }
      ]
    },
    {
      path:"User",
      component:UserComponent,
      children:[
        {
          path: 'View/:id', 
          component:UserFormComponent,
          data:{
            mode:"VIEW"
          }
        },
      ]
    }
    
  ]}
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgotPasswordComponent,
    NewAccountComponent,
    HomeComponent,
    EmbassyComponent,
    EmbassyListComponent,
    EmbassyFormComponent,
    EmbassyMembersComponent,
    HoldableDirective,
    UserFormComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    FormsModule,
    HttpClientModule, 
  ],
  providers: [EmbassyService,UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
