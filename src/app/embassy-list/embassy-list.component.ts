import { Component, OnInit } from '@angular/core';
import { EmbassyService } from '../embassy.service';

@Component({
  selector: 'app-embassy-list',
  templateUrl: './embassy-list.component.html',
  styleUrls: ['./embassy-list.component.scss']
})
export class EmbassyListComponent implements OnInit {

  constructor(private _service : EmbassyService) { }
  contentList = []



  ngOnInit() {
    this._service.getEmbassyByUser("5c81ed38fac7608022e66765").then((items : any) => {
      this.contentList = items
    }).catch(err => {
      alert(JSON.stringify(err))
    })
  }

}
