import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbassyListComponent } from './embassy-list.component';

describe('EmbassyListComponent', () => {
  let component: EmbassyListComponent;
  let fixture: ComponentFixture<EmbassyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbassyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbassyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
