import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  constructor(
    private _service : UserService,
    private route : ActivatedRoute,
  ) { }

  content

  ngOnInit() {
    this.content = {}
    this.route.params.subscribe(params => {
      this._service.getUserById(params.id).then((content:any) => {
        this.content = content
      }).catch(err => {
        console.log(err)
      })
    })
  }

}
