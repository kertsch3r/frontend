import { TestBed } from '@angular/core/testing';

import { EmbassyService } from './embassy.service';

describe('EmbassyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmbassyService = TestBed.get(EmbassyService);
    expect(service).toBeTruthy();
  });
});
