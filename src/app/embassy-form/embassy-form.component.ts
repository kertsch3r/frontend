import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmbassyService } from '../embassy.service';

@Component({
  selector: 'app-embassy-form',
  templateUrl: './embassy-form.component.html',
  styleUrls: ['./embassy-form.component.scss']
})
export class EmbassyFormComponent implements OnInit {

  constructor(
    private _service : EmbassyService,
    private route : ActivatedRoute,
    private _router: Router,
  ) { }
  content
  viewMode
  ngOnInit() {
    this.content = {}
    this.route.data.subscribe(dataItems => {
      this.viewMode = dataItems.mode
      if(this.viewMode == "INSERT"){

      }else{
        this.route.params.subscribe(params => {
          this._service.getEmbassyById(params.id).then((content:any) => {
            this.content = content
          }).catch(err => {
            console.log(err)
          })
        })
      }
    })
  }

}
