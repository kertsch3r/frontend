import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbassyMembersComponent } from './embassy-members.component';

describe('EmbassyMembersComponent', () => {
  let component: EmbassyMembersComponent;
  let fixture: ComponentFixture<EmbassyMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbassyMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbassyMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
