import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {HoldableDirective} from '../holdable.directive'
import { EmbassyService } from '../embassy.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-embassy-members',
  templateUrl: './embassy-members.component.html',
  styleUrls: ['./embassy-members.component.scss']
})
export class EmbassyMembersComponent implements OnInit {

  @ViewChild('openButton') openModal : ElementRef
  @ViewChild('closeButton') closeButton : ElementRef


  constructor(
    private _service : EmbassyService,
    private route : ActivatedRoute,
    private _router : Router
  ) { }
  content
  member
  ngOnInit() {
    this.content = {}
    this.member = {}
    this.route.params.subscribe(params => {
      this._service.getEmbassyMembers(params.id).then((content:any) => {
        this.content = content
      }).catch(err => {
        console.log(err)
      })
    })
  }


  openIteractionMenu(item){
    this.member = item
    this.openModal.nativeElement.click()
  }

  viewMember(id){
    this.closeButton.nativeElement.click()
    this._router.navigateByUrl(`/App/User/View/${id}`);
  }
}
