import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmbassyService {

  constructor(private _http: HttpClient) { }


  
  getEmbassyList(){
    var self = this
    return new Promise((resolve,reject) => {
      //  let sessionToken = this._session.currentSession.token 
        self._http.get(`${environment.API_URL}/App/Embassy`).subscribe(done => {
          resolve(done)
        },err=> {
          reject(err)
        })
    })
  }

  insertEmbassy(payload){
    var self =this 
    return new Promise((resolve,reject) => {
      self._http.post(`${environment.API_URL}/App/Embassy`,payload).subscribe(done => {
          resolve(done)
        },err=> {
          reject(err)
        })
    })
  }

  getEmbassyMembers(id){
    var self =this
    return new Promise((resolve,reject) => {
      self._http.get(`${environment.API_URL}/App/Embassy/Members/${id}`).subscribe(done => {
        resolve(done)
      },err=> {
        reject(err)
      })
    })
  }

  getEmbassyByUser(id){
    var self =this
    return new Promise((resolve,reject) => {
      self._http.get(`${environment.API_URL}/App/Embassy/ByUser/${id}`).subscribe(done => {
        resolve(done)
      },err=> {
        reject(err)
      })
    })
  }

  getEmbassyById(id){
    var self =this
    return new Promise((resolve,reject) => {
      self._http.get(`${environment.API_URL}/App/Embassy/ById/${id}`).subscribe(done => {
        resolve(done)
      },err=> {
        reject(err)
      })
    })
  }


  updateEmbassy(id,payload){
    var self =this 
    return new Promise((resolve,reject) => {
      self._http.put(`${environment.API_URL}/App/Embassy/${id}`,payload).subscribe(done => {
          resolve(done)
        },err=> {
          reject(err)
        })
    })
  }


  removeEmbassy(id){
    var self =this 
    return new Promise((resolve,reject) => {
      self._http.delete(`${environment.API_URL}/App/Embassy/${id}`).subscribe(done => {
          resolve(done)
        },err=> {
          reject(err)
        })
    })
  }
}
