import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: HttpClient) { }

  getUserById(id){
    var self =this
    return new Promise((resolve,reject) => {
      self._http.get(`${environment.API_URL}/App/User/ById/${id}`).subscribe(done => {
        resolve(done)
      },err=> {
        reject(err)
      })
    })
  }

}
